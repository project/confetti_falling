# Confetti Falling

Confetti falling animation is a visual effect that involves the simulation of
small pieces of paper or other materials falling from the top of a screen or
display. This animation is often used to add a festive or celebratory touch to
websites, videos, or other digital content. It can be created using computer
graphics techniques, such as particle systems, to generate realistic-looking
confetti that moves and interacts with its surroundings in a natural way. The
appearance and behavior of the confetti can be customized, such as by adjusting
its size, shape, color, and speed. This animation can be implemented on a Drupal
website using a module that adds the confetti-falling animation effect to any
page on the site.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/confetti_falling).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/confetti_falling).


## Table of contents

- Requirements
- Installation
- Configuration
- Usage
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The Module have only small configuration form there add add classname for show you 
confetti falling animation (`/admin/config/confetti_falling_settings`)


## Usage

- Confetti Falling effect to your site as mention class name.
- After Set Classname in Configuration form (/admin/config/confetti_falling_settings)
  get this Confetti Falling animation effect on give classname page.


## Maintainers

- Omprakash Mankar - [omrmankar](https://www.drupal.org/u/omrmankar)