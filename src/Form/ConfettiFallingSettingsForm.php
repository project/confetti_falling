<?php

namespace Drupal\confetti_falling\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ConfettiFallingSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'confetti_falling_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'confetti_falling.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('confetti_falling.settings');
    $form['class_name'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Enter Class or ID of Page'),
      '#description' => $this->t('Enter a class name like e.g .page-node-type-article, .path-frontpage'),
      '#default_value' => $config->get('class_name'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('confetti_falling.settings')
      ->set('class_name', $form_state->getValue('class_name'))
      ->save();

  }

}
